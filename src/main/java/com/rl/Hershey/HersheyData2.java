package com.rl.Hershey;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class HersheyData2 {

	static Connection conn = null;

	public static void main(String[]  args){
		
		
		 	try{
		 		String jdbcUrl = "jdbc:postgresql://192.168.101.42/crystal";
				String username = "postgres";
				String password = "postgres!@#";

				conn = DriverManager.getConnection(jdbcUrl, username, password);
				 List<String> files = new ArrayList<String>();
				 Statement stmt1=null;
				 ResultSet rs1=null;
				 Statement stmt2=null;
				 ResultSet rs2=null;
				 PreparedStatement ps1=null;

				String sql2="INSERT INTO boa.WEIGHT_CONTROL_REQUIREMENTS_TEMP(file_name ,ITEM ,DECLARED_WT ,MAX_ALLOWABLE_VAR ,TARGET_WT ,DEFECTIVE_WT ,Weight_Notation ,Where_Sold ,Sales_Note) VALUES (?,?,?,?,?,?,?,?,?)";
				 PreparedStatement ps2 = null;
			ps1 = conn.prepareStatement(sql2);
			stmt1 = conn.createStatement();
			rs1=stmt1.executeQuery("select distinct(file_name) from boa.hershey_data_dump where file_name not in ('Natural Process Cocoa Powder.xlsx') order by file_name");

			while(rs1.next()){
				files.add(rs1.getString("file_name"));
			}
			
			for(String file1:files){
				//System.out.println(file1);
				
				boolean getrwIndex=true;
				boolean forSoldIn=true;
				boolean soldNotthere=true;
				int executionCount=0;
				int swap=0;

				stmt2=conn.createStatement();
				rs2=stmt2.executeQuery("select file_name,raw_text,row_index,col_index,row_height,col_span,font_bold,font_underline from  boa.hershey_data_dump where file_name='"+file1.replaceAll("'", "''")+"' order by row_index,col_index");
				StringBuilder sbr=new StringBuilder();

				List<Double>  colIndexCapture=new ArrayList<Double>();
				List<String> textCapture= new ArrayList<String>();
				HersheyDataVO hdvo=new HersheyDataVO();
				
				
				while(rs2.next()){
					
					int	count=0;

					String fileName=(rs2.getString("file_name"));
					String rawText=(rs2.getString("raw_text"));
					int    rowIndex=(rs2.getInt("row_index"));
					double    colIndex=(rs2.getInt("col_index"));
					double rowHeight=(rs2.getDouble("row_height"));
					int colSpan=(rs2.getInt("col_span"));
					String fontBold=(rs2.getString("font_bold"));
					String fontUnderline=(rs2.getString("font_underline"));
					

					String pattern="Items Sold in ";
					String pattern2="Items sold in";
					String dfWght="Defective Weight";

					if ("WEIGHT CONTROL REQUIREMENTS_".equals(rawText.trim().toUpperCase())) {
						forSoldIn = false;
						swap = 1;
					}
					if ("WEIGHT CONTROL REQUIREMENTS".equals(rawText.trim().toUpperCase())) {
						forSoldIn = false;
						swap = 1;
					}

					if ("NET WEIGHT".equalsIgnoreCase(rawText.trim().toUpperCase())) {
						forSoldIn = false;
						swap = 1;
					}
					
					if(forSoldIn==false){
						
						
							if(swap!=1){
							if ((rawText.trim()).startsWith(pattern.trim()) || (rawText.trim()).startsWith(pattern2.trim())) {
								hdvo.setWhereSold(rawText);
								//executionCount++;
							} else if (rowHeight == 14.25) {
								sbr.append(rawText);
							} else if (rowHeight == 15) {
								forSoldIn = true;
								hdvo.setSalesNote(sbr.toString());
								//executionCount++;
							}
							}
						
							
						if ("DEFECTIVE WT.".equals(rawText.trim().toUpperCase())
								|| (rawText.trim()).startsWith(dfWght.trim())) {
							getrwIndex = false;
							count = 1;
						}
					}
					
					if (getrwIndex == false) {
						if (count == 1) {
							hdvo.setFileName(fileName);
						} else if(rowHeight==14.25) {
							if(colIndex==1 && colSpan!=5){					//one solved
								hdvo.setItem(rawText);
							}
							else if(colIndex==2){				//two
								hdvo.setDeclaredWeight(rawText);
							}
							else if(colIndex==3){
								hdvo.setMaxAllowableVar(rawText);
							}
							else if(colIndex==4){
								hdvo.setTargetWeight(rawText);
							}
							else if (colIndex==5){
								hdvo.setDefectiveWeight(rawText);
							}
							else if (colSpan==5 && colIndex==1){
								hdvo.setWeightNotation(rawText);
							}
						}
					}
					
					
				}
				ps1.executeBatch();
			}
			
			
			
		 	}
		 
		 	
		 	
		 	
		 	catch(Exception e){
		 		e.printStackTrace();
		 	}
		
	}
	
	
	
}
