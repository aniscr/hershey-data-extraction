package com.rl.Hershey;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.rl.Hershey.*;

public class WeightControlGroups {

	
    static Connection conn = null;

	public static void main(String[] args) {

		String jdbcUrl = "jdbc:postgresql://192.168.101.42/crystal";
		String username = "postgres";
		String password = "postgres!@#";
		
		try {
			conn = DriverManager.getConnection(jdbcUrl, username, password);
			
			group1(conn);
			group2(conn);
			group3(conn);
			
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}

	
	public static void group1(Connection conn1){
		
		String fileName="";
		String sold1="Items sold in";
		String sold2="Items Sold in";
		String sold3="ITEMS SOLD IN";
		String sold4="Products Sold in";
		String entry1="WEIGHT CONTROL REQUIREMENTS";
		String entry2="WEIGHT CONTROL REQUIREMENTS_";
		String entry3="NET WEIGHT";
		
		int te=0;
		int weight=0;
		boolean e1 = false;
		boolean e2 = false;
		int counter = 0;
		Statement stmt1 = null;
		ResultSet rs1 = null;
		PreparedStatement ps1 = null;
		
		 try{
	    File path = new File("D:\\Harshey\\grouped_files\\group3.txt");
		BufferedReader br = new BufferedReader(new FileReader(path));
		List<Integer> colIndexList=new ArrayList<Integer>();
		
		while((fileName=br.readLine())!=null){
			
			stmt1=conn1.createStatement();
			rs1=stmt1.executeQuery("select * from boa.hershey_data_dump where file_name='"+fileName.replaceAll("'", "''")+"' order by row_index,col_index");
			String sql1="INSERT INTO boa.WEIGHT_CONTROL_REQUIREMENTS(file_name ,ITEM ,DECLARED_WT ,MAX_ALLOWABLE_VAR ,TARGET_WT ,DEFECTIVE_WT ,Weight_Notation ,Where_Sold ,Sales_Note) VALUES (?,?,?,?,?,?,?,?,?)";
			ps1 = conn.prepareStatement(sql1);
			StringBuilder sbr=new StringBuilder();

			
				while (rs1.next()) {
					String rawText = rs1.getString("raw_text");
					int colIndex = rs1.getInt("col_index");
					double rowHeight = rs1.getDouble("row_height");
					String fontUnderline = rs1.getString("font_underline");
					
					ps1.setString(1, fileName);

					if(rawText.trim().equalsIgnoreCase(entry1.trim()) || rawText.trim().equalsIgnoreCase(entry2.trim()) ||rawText.trim().equalsIgnoreCase(entry3.trim())  ){
						e1=true;
						weight=1;
					}
					if(rawText.trim().equalsIgnoreCase("DEFECTIVE WT.") || rawText.trim().equalsIgnoreCase("Defective Weight") ||rawText.trim().equalsIgnoreCase("DEFECTIVE WT.*") ||rawText.trim().equalsIgnoreCase("DEFECTIVE WT")){
						e2=true;
						te=1;
					}
					if ((rawText).startsWith(sold1)  || (rawText).startsWith(sold2) || (rawText).startsWith(sold3) || (rawText).startsWith(sold4)){
						e1=true;
					}
					
					
					if(e1==true){
						if(weight==1){
							weight=0;
							continue;
						}
						if ((rawText).startsWith(sold1)  || (rawText).startsWith(sold2) || (rawText).startsWith(sold3) || (rawText).startsWith(sold4)){
							ps1.setString(8, rawText);
						}
						else if(rowHeight==14.25){
							sbr.append(rawText);
						}
						else if(rowHeight==15 && (fontUnderline.trim()).equals("-4142") || rowHeight==30 && (fontUnderline.trim()).equals("-4142")){
							ps1.setString(9, sbr.toString());
							e1=false;
							sbr=new StringBuilder();
						}
					}
					
					if(e2==true){
						if(te==1){
							te=0;
							continue;
						}
						else if(colIndex==1){
							ps1.setString(2, rawText);
							colIndexList.add(colIndex);
							counter++;
						}
						else if(colIndex==2){
							ps1.setString(3, rawText);
							if(counter!=1){
								counter++;
							}
							colIndexList.add(colIndex);
							counter++;
						}else if(colIndex==3){
							ps1.setString(4, rawText);
							colIndexList.add(colIndex);
							counter++;
						}else if(colIndex==4){
							ps1.setString(5, rawText);
							colIndexList.add(colIndex);
							counter++;
						}else if(colIndex==5){
							ps1.setString(6, rawText);
							colIndexList.add(colIndex);
							counter++;
						}
					}
					
					if(counter==5){
						if(colIndexList.get(0)!=1){
							ps1.setString(2, "");
						}
						ps1.setString(7, "");
						ps1.addBatch();
						ps1.executeBatch();
						ps1.clearBatch();
						counter = 0;
						colIndexList.clear();
						e2=false;
					}
					
				}
		}
		
		System.out.println("group 1 is done");
		
		 }
		 catch(Exception e){
			 e.printStackTrace();
		 }
		
	}
	
	public static void group2(Connection conn2) throws SQLException, IOException{
		
		String sql2="INSERT INTO boa.WEIGHT_CONTROL_REQUIREMENTS(file_name ,ITEM ,DECLARED_WT ,MAX_ALLOWABLE_VAR ,TARGET_WT ,DEFECTIVE_WT ,Weight_Notation ,Where_Sold ,Sales_Note) VALUES (?,?,?,?,?,?,?,?,?)";
		PreparedStatement ps2 = null;
		Statement stmt1 = null;
		ResultSet rs1 = null;
		ps2 = conn.prepareStatement(sql2);

		File path = new File("D:\\Harshey\\grouped_files\\group1.txt");
		BufferedReader br = new BufferedReader(new FileReader(path));
		List<String> files = new ArrayList<String>();
			
		String line="";

			while((line=br.readLine())!=null){
				files.add(line);
			}
			
			for(String file:files){
				
				boolean getrwIndex=true;
				boolean forSoldIn=true;
				int executionCount=0;
				int swap=0;

				stmt1=conn.createStatement();
				rs1=stmt1.executeQuery("select * from  boa.hershey_data_dump where file_name='"+file.replaceAll("'", "''")+"' order by row_index,col_index");
				StringBuilder sbr=new StringBuilder();

				List<Double>  colIndexCapture=new ArrayList<Double>();
				List<String> textCapture= new ArrayList<String>();
				
				while(rs1.next()){
					
					int	count=0;

				String fileName = (rs1.getString("file_name"));
				String rawText = (rs1.getString("raw_text"));
				double colIndex = (rs1.getInt("col_index"));
				double rowHeight = (rs1.getDouble("row_height"));
				int colSpan = (rs1.getInt("col_span"));
				String borderStyle = (rs1.getString("border_style"));

				String dfWght = "Defective Weight";
				String pattern = "Items Sold in ";
				String pattern2 = "Items sold in";
				String pattern3 = "ITEMS SOLD IN";
				String pattern4 = "Products Sold in";
					
				if ((rawText.trim()).startsWith(pattern.trim()) || (rawText.trim()).startsWith(pattern2.trim())
						|| (rawText.trim().toUpperCase()).startsWith(pattern3.trim())
						|| (rawText.trim()).startsWith(pattern4.trim())) {
					forSoldIn = false;
				}
					
				if ("WEIGHT CONTROL REQUIREMENTS_".equals(rawText.trim().toUpperCase())) {
					forSoldIn = false;
					swap = 1;
				}
				if ("WEIGHT CONTROL REQUIREMENTS".equals(rawText.trim().toUpperCase())) {
					forSoldIn = false;
					swap = 1;
				}
				if ("NET WEIGHT".equalsIgnoreCase(rawText.trim().toUpperCase())) {
					forSoldIn = false;
					swap = 1;
				}
					
				if (forSoldIn == false) {
					if ((rawText.trim()).startsWith(pattern.trim()) || (rawText.trim()).startsWith(pattern2.trim()) || (rawText.trim().toUpperCase()).startsWith(pattern3.trim()) || (rawText.trim()).startsWith(pattern4.trim())) {
						ps2.setString(8, rawText);
						executionCount++;
					} else if (rowHeight == 14.25) {
						sbr.append(rawText);
					} else if (rowHeight == 15 && borderStyle.equals("1") || rowHeight == 30 && borderStyle.equals("1")) {
						forSoldIn = true;
						ps2.setString(9, sbr.toString());
						executionCount++;
					}
				}
					
			
				if ("DEFECTIVE WT.".equals(rawText.trim().toUpperCase()) || (rawText.trim()).startsWith(dfWght.trim()) || "DEFECTIVE WT".equals(rawText.trim().toUpperCase())) {
					getrwIndex = false;
					count = 1;
				}

				if (getrwIndex == false) {
					if (count == 1) {
						ps2.setString(1, fileName);
						executionCount++;
						continue;
					} else if (rowHeight == 14.25 || rowHeight == 12.75 && borderStyle.equals("#NULL#")) {
						colIndexCapture.add(colIndex);
						if (colIndex == 1 && colSpan != 5 && colSpan != 4) {
							ps2.setString(2, rawText);
							executionCount++;
						} else if (colIndex == 2) {
							ps2.setString(3, rawText);
							executionCount++;
							if (executionCount != 5) {
								executionCount++;
							}
						} else if (colIndex == 3) {
							ps2.setString(4, rawText);
							executionCount++;
							if (executionCount != 6) {
								executionCount++;
							}
						} else if (colIndex == 4) {
							ps2.setString(5, rawText);
							executionCount++;
							if (executionCount != 7) {
								executionCount++;
							}
						} else if (colIndex == 5) {
							ps2.setString(6, rawText);
							executionCount++;
							if (executionCount != 8) {
								executionCount++;
							}
						} else if (colSpan == 5 && colIndex == 1 || colSpan == 4 && colIndex == 1) {
							ps2.setString(7, rawText);
							executionCount++;
							if (executionCount != 9) {
								executionCount++;
							}
						}
					}
				}
				
				if (executionCount == 9) {

					if (colIndexCapture.get(0) != 1) {
						ps2.setString(2, "");
					}
					if (colIndexCapture.size() <= 5) {
						if (colIndexCapture.get(0) == 2 && colIndexCapture.get(1) == 4 || colIndexCapture.get(3) != 5) {
							ps2.setString(6, "");
						}
					}
					ps2.addBatch();
					ps2.executeBatch();
					ps2.clearBatch();
					executionCount = 0;
					getrwIndex = true;
				}
			}
		}
			System.out.println("group 2 is done");
	}
	
	public static void group3(Connection conn){
		
		String line="";
		String sql2="INSERT INTO boa.WEIGHT_CONTROL_REQUIREMENTS(file_name ,ITEM ,DECLARED_WT ,MAX_ALLOWABLE_VAR ,TARGET_WT ,DEFECTIVE_WT ,Weight_Notation ,Where_Sold ,Sales_Note) VALUES (?,?,?,?,?,?,?,?,?)";
		PreparedStatement ps2 = null;
	    Statement stmt1=null;
	    ResultSet rs1=null;
		
	    try {
			ps2 = conn.prepareStatement(sql2);
			File path = new File("D:\\Harshey\\grouped_files\\group7.txt");
			BufferedReader br = new BufferedReader(new FileReader(path));
			List<String> files = new ArrayList<String>();
				
				
				while((line=br.readLine())!=null){
					files.add(line);
				}
				
				for(String file:files){
					
					stmt1=conn.createStatement();
					rs1=stmt1.executeQuery("select * from  boa.hershey_data_dump where file_name='"+file.replaceAll("'", "''")+"' order by row_index,col_index");
					
					
				}
				
				
				
			} catch (SQLException e) {
				e.printStackTrace();
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			
		
			
		
		
	}
}
