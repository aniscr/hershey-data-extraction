package com.rl.Hershey;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import com.rl.*;

public class HersheyData {

	static List<String> files = new ArrayList<String>();
	static List<String> files2 = new ArrayList<String>();
	static List<String> missedFiles = new ArrayList<String>();


	static Connection conn = null;
	static Statement stmt1 = null;
	static ResultSet rs1 = null;
	static Statement stmt2 = null;
	static ResultSet rs2 = null;
	static Statement stmt3 = null;
	static ResultSet rs3 = null;
	static Statement stmt4 = null;
	static ResultSet rs4 = null;

	public static void main(String[] args) throws SQLException {

		String jdbcUrl = "jdbc:postgresql://192.168.101.42/crystal";
		String username = "postgres";
		String password = "postgres!@#";

		conn = DriverManager.getConnection(jdbcUrl, username, password);
		//weightScreen(conn);
		//HersheyData2 hd2=new HersheyData2();
		//hd2.weightControlRequirements(conn);
		/*pAndp2(conn);

		System.out.println("Connection Established loading data........");
		String sql = "INSERT INTO boa.product_and_packaging_appearance_standards(file_name,Quality_Standards_and_Tolerances) VALUES (?,?)";

		Statement stmt = null;
		ResultSet rs = null;
		PreparedStatement ps = null;
		ps = conn.prepareStatement(sql);
		stmt = conn.createStatement();
		rs=stmt.executeQuery("select distinct(file_name) from boa.hershey_data_dump  where file_name not in ('Hershey''s Extra Creamy Solid Eggs for Easter.xlsx','Hershey''s Milk Chocolate Solid Egg for Easter.xlsx')");
		
		while(rs.next()){
			files.add(rs.getString("file_name"));
		}
		
		for (String file : files) {

			boolean getrxIndex = true;
			StringBuilder sbr = new StringBuilder();
			stmt1 = conn.createStatement();
			rs1 = stmt1.executeQuery("select file_name,raw_text,row_index,row_height from boa.hershey_data_dump where file_name='"
							+ file.replaceAll("'", "''") + "'");

			String fileName2 = "";
			int stp=0;
			while (rs1.next()) {

				int rwind = 0;

				String fileName1 = rs1.getString("file_name");
				String rawText1 = rs1.getString("raw_text");
				int rowIndex1 = rs1.getInt("row_index");
				double rowHeight = rs1.getDouble("row_height");

				if ("PRODUCT AND PACKAGING APPEARANCE STANDARDS_".equals(rawText1.toUpperCase())
						|| "PRODUCT AND PACKAGING APPEARANCE STANDARDS".equals(rawText1.toUpperCase())) {
					getrxIndex = false;
					rwind++;
				}

				if (getrxIndex == false) {

					if (!fileName1.equalsIgnoreCase(fileName2)) {
						ps.setString(1, fileName1);
					} else {
						continue;
					}

					if (rowHeight == 14.25) {
						if (rwind == 1) {
							continue;
						} else if ("See Quality Standards and Tolerances".equalsIgnoreCase(rawText1)) {
							continue;
						} else if (rwind == 0) {
							if (rawText1.startsWith("A.	") || rawText1.startsWith("B.	")
									|| rawText1.startsWith("C.	")) {
								sbr.append("\n");
								sbr.append(rawText1);
							} else {
								sbr.append(rawText1);
								stp = 1;
							}
						}
					}

					if (rowHeight == 12.75) {
						getrxIndex = true;
						if (stp == 1) {
							// checkAllFiles.add(fileName1);
							ps.setString(2, sbr.toString());
							ps.addBatch();
							ps.executeBatch();
							ps.clearBatch();
						}
					}
				}
			}
		}
		*/
		
		// second screen  
		
		String sql2="INSERT INTO boa.WEIGHT_CONTROL_REQUIREMENTS(file_name ,ITEM ,DECLARED_WT ,MAX_ALLOWABLE_VAR ,TARGET_WT ,DEFECTIVE_WT ,Weight_Notation ,Where_Sold ,Sales_Note) VALUES (?,?,?,?,?,?,?,?,?)";
		 PreparedStatement ps2 = null;

			ps2 = conn.prepareStatement(sql2);
			stmt3 = conn.createStatement();
			rs3=stmt3.executeQuery("select distinct(file_name) from boa.hershey_data_dump where file_name not in ('Natural Process Cocoa Powder.xlsx') order by file_name");

			while(rs3.next()){
				files2.add(rs3.getString("file_name"));
			}
			
			for(String file1:files2){
				//System.out.println(file1);
				
				boolean getrwIndex=true;
				boolean forSoldIn=true;
				boolean soldNotthere=true;
				int executionCount=0;
				int swap=0;

				stmt4=conn.createStatement();
				rs4=stmt4.executeQuery("select file_name,raw_text,row_index,col_index,row_height,col_span,font_bold,font_underline from  boa.hershey_data_dump where file_name='"+file1.replaceAll("'", "''")+"' order by row_index,col_index");
				StringBuilder sbr=new StringBuilder();

				List<Double>  colIndexCapture=new ArrayList<Double>();
				List<String> textCapture= new ArrayList<String>();
				
				while(rs4.next()){
					
					int	count=0;

					String fileName=(rs4.getString("file_name"));
					String rawText=(rs4.getString("raw_text"));
					int    rowIndex=(rs4.getInt("row_index"));
					double    colIndex=(rs4.getInt("col_index"));
					double rowHeight=(rs4.getDouble("row_height"));
					String dfWght="Defective Weight";
					int colSpan=(rs4.getInt("col_span"));
					String fontBold=(rs4.getString("font_bold"));
					String fontUnderline=(rs4.getString("font_underline"));

					
					
					String pattern="Items Sold in ";
					String pattern2="Items sold in";
					
					if(fileName.equalsIgnoreCase("Bar, Hershey Milk, Golden Almond.xlsx")){
						System.out.println("got it");
						System.out.println(rowIndex);
					}
					
				//	List<String> textCapture= new ArrayList<String>();
				//	List<String>  rawData=new ArrayList<String>();
					
				if ((rawText.trim()).startsWith(pattern.trim())  || (rawText.trim()).startsWith(pattern2.trim()) ){//|| (rawText.trim()).startsWith(tbName.trim())) {
					forSoldIn=false;
					soldNotthere=false;
				}

					
					if("WEIGHT CONTROL REQUIREMENTS_".equals(rawText.trim().toUpperCase()) ){
						forSoldIn=false;
						swap=1;
					}
					if("WEIGHT CONTROL REQUIREMENTS".equals(rawText.trim().toUpperCase()) ){
						forSoldIn=false;
						swap=1;
					}
					
					if("NET WEIGHT".equalsIgnoreCase(rawText.trim().toUpperCase())){
						forSoldIn=false;
						swap=1;
						
					}
					
				if(soldNotthere==true){
					ps2.setString(7, "");
					ps2.setString(8, "");
					ps2.setString(9, "");
				}
				
				if (forSoldIn == false) {
					if(swap!=1){
					if ((rawText.trim()).startsWith(pattern.trim()) || (rawText.trim()).startsWith(pattern2.trim())) {
						ps2.setString(8, rawText);
						executionCount++;
					} else if (rowHeight == 14.25) {
						sbr.append(rawText);
					} else if (rowHeight == 15) {
						forSoldIn = true;
						ps2.setString(9, sbr.toString());
						executionCount++;
					}
					}
				}
					
			
				if ("DEFECTIVE WT.".equals(rawText.trim().toUpperCase()) || (rawText.trim()).startsWith(dfWght.trim())) {
					getrwIndex = false;
					count = 1;
				}

				if (getrwIndex == false) {
					if (count == 1) {
						ps2.setString(1,fileName);
						executionCount++;
						continue;
					} else if(rowHeight==14.25) {
						colIndexCapture.add(colIndex);
						if(colIndex==1 && colSpan!=5){					//one solved
							ps2.setString(2, rawText);
							executionCount++;
						}
						else if(colIndex==2){				//two
							ps2.setString(3, rawText);
							executionCount++;
							if(executionCount!=5){
								executionCount++;
							}
						}
						else if(colIndex==3){
							ps2.setString(4, rawText);
							executionCount++;
							if(executionCount!=6){
								executionCount++;
							}
						}
						else if(colIndex==4){
							ps2.setString(5, rawText);
							executionCount++;
							if(executionCount!=7){
								executionCount++;
							}
						}
						else if (colIndex==5){
							ps2.setString(6, rawText);
							executionCount++;
							if(executionCount!=8){
								executionCount++;
							}
						}
						else if (colSpan==5 && colIndex==1){
							ps2.setString(7, rawText);
							executionCount++;
							if(executionCount!=9){
								executionCount++;
							}
						}
					}
				}
				
				
				
				if (executionCount == 9) {
					
					if (colIndexCapture.get(0) != 1) {
						ps2.setString(2, "");
					}
					if (colIndexCapture.size() <= 5) {
						if (colIndexCapture.get(0) == 2 && colIndexCapture.get(1) == 4 || colIndexCapture.get(3) != 5) {
							ps2.setString(6, "");
						}
					}
					ps2.addBatch();
					ps2.executeBatch();
					ps2.clearBatch();
					executionCount = 0;
					getrwIndex = true;
				}
			}
				
				
		}
			
		
		
		/* String sql2="INSERT INTO boa.WEIGHT_CONTROL_REQUIREMENTS(file_name,ITEM,DECLARED_WT,MAX_ALLOWABLE_VAR,TARGET_WT,DEFECTIVE_WT) VALUES (?,?,?,?,?,?)";
		 PreparedStatement ps2 = null;

			ps2 = conn.prepareStatement(sql2);
			stmt3 = conn.createStatement();
			//rs3=stmt3.executeQuery("select * from boa.hershey_data_dump where raw_text='DECLARED WT.'");
			rs3=stmt3.executeQuery("select distinct(file_name) from boa.hershey_data_dump order by file_name");

			while(rs3.next()){
				files2.add(rs3.getString("file_name"));
			}
			int counter=0;
			for(String fls:files2){
				
				
				//missedFiles.add(fls);
				counter++;
				int getrwIndex=0;
				stmt4=conn.createStatement();
				rs4=stmt4.executeQuery("select file_name,raw_text,row_index,col_index,row_height from  boa.hershey_data_dump where file_name='"+fls.replaceAll("'", "''")+"' order by file_name,row_index");
			
				List<Integer> captureColIndex= new ArrayList<Integer>();
				List<String>  colCheck=new ArrayList<String>();
			//	List<String>  rawData=new ArrayList<String>();

			
				int executeCounter=0;
				while(rs4.next()){
					
										
					String fileName=(rs4.getString("file_name"));
					String rawText=(rs4.getString("raw_text"));
					int    rowIndex=(rs4.getInt("row_index"));
					int    colIndex=(rs4.getInt("col_index"));
				
					 

					if("WEIGHT CONTROL REQUIREMENTS".equals(rawText.trim().toUpperCase()) ){
						getrwIndex=rowIndex+10;
					}
					
					if("NET WEIGHT".equalsIgnoreCase(rawText.trim().toUpperCase())){
						getrwIndex=rowIndex+3;
					}
					
					if(fileName.equalsIgnoreCase("Cocont Wte Bite.xlsx")){
						System.out.println("got it");
					}
					
					
				if (rowIndex == getrwIndex) {
					
				//	rawData.add(rawText);
				
					
					System.out.println(colCheck.size() +"  "+fileName);
					ps2.setString(1, fileName);
					executeCounter++;
					
					 
					captureColIndex.add(colIndex);

					if (colIndex == 1) {
						ps2.setString(2, rawText);
					}

					if (colIndex == 2) {
						ps2.setString(3, rawText);
					}

					if (colIndex == 3) {
						ps2.setString(4, rawText);
					}
					

					if (colIndex == 4) {
						ps2.setString(5, rawText);
						if (executeCounter == 4) {
							executeCounter++;
							ps2.setString(6, "");
						}
					}
					
					
					

					if (colIndex == 5) {
						ps2.setString(6, rawText);
						if (executeCounter == 4) {
							executeCounter++;
						}
						if (executeCounter == 3) {
							executeCounter++;
							executeCounter++;
						}
					}

					if (executeCounter == 5 ) {
						if ((captureColIndex.get(0)) != 1) {
							ps2.setString(2, "");
						}
						ps2.addBatch();
						ps2.executeBatch();
						ps2.clearBatch();
						executeCounter = 0;
					}
				}
				}
				
				
				
			}
			
			System.out.println(counter+" files have been Processed and loaded to data base");*/
	 }
	
	
	/*public static void pAndp2(Connection conn2) throws SQLException{
		
		
		String sql = "INSERT INTO boa.product_and_packaging_appearance_standards(file_name,Quality_Standards_and_Tolerances,attribute,target_acceptable,need_improvement,unacceptable) VALUES (?,?,?,?,?,?)";
		Statement stmt2 = null;
		ResultSet rs2 = null;
		PreparedStatement ps1 = null;
		ps1 = conn2.prepareStatement(sql);
		List<String> fileslist = new ArrayList<String>();
		try {
			Statement stmt = conn2.createStatement();
			ResultSet rs = stmt.executeQuery("select distinct(file_name) from boa.hershey_data_dump order by file_name");
			while (rs.next()) {
				fileslist.add(rs.getString(1));
			}
			 
			 
			 for(String file:fileslist){

				boolean start = true;
				boolean cd = true;
				// int difcounter=0;

				List<String> difcount = new ArrayList<String>();
				stmt2 = conn2.createStatement();
				 rs2=stmt2.executeQuery("select file_name,raw_text,row_index,col_index,row_height,font_bold from boa.hershey_data_dump where file_name='"+file.replaceAll("'", "''")+"' order by row_index,col_index");
	
			 while(rs2.next()){
				 
				
					String fileName = (rs2.getString("file_name"));
					String rawText = (rs2.getString("raw_text"));
					String fontBold = (rs2.getString("font_bold"));
					int rowIndex = (rs2.getInt("row_index"));
					int colIndex = (rs2.getInt("col_index"));
					double row_height = (rs2.getDouble("row_height"));
					
					String s1 = "Chip Discoloration";
					String s2 = "Foreign Material";
					String s3 = "Piece Damage";
					String ss1 = "Chip Count";
					String ss2 = "Chip Shape";
					if (fileName.equals("Assort, Bridge Mix.xlsx")) {
						break;
					}

					if (row_height == 199.5 && (ss1.trim()).equals(rawText.trim())  || row_height == 171 && (ss1.trim()).equals(rawText.trim()) || row_height == 42.75 && (ss1.trim()).equals(rawText.trim()) ||row_height == 185.25 && (ss1.trim()).equals(rawText.trim())) {
						start = false;
					}
					if (row_height == 99.75 && (ss2.trim()).equals(rawText.trim()) || row_height == 85.5 && (ss2.trim()).equals(rawText.trim())) {
						start = false;
					}

					if (start == false) {

						if (rawText.contains("N=50")) {
							continue;
						}

						ps1.setString(1, fileName);
						ps1.setString(2, "");

						if (colIndex == 1) {
							ps1.setString(3, rawText);
							// difcounter++;
							difcount.add(rawText);
						}
						if (colIndex == 2) {
							ps1.setString(4, rawText);
							// difcounter++;
							difcount.add(rawText);
						}
						if (colIndex == 3) {
							ps1.setString(5, rawText);
							// difcounter++;
							difcount.add(rawText);
						}
						if (colIndex == 4) {
							ps1.setString(6, rawText);
							// difcounter++;
							difcount.add(rawText);
						}

						if (difcount.size() == 4) {
							ps1.addBatch();
							ps1.executeBatch();
							ps1.clearBatch();
							// difcounter=0;
							difcount = new ArrayList<String>();
							start = true;
						}
					}

					if ((s1.trim()).equalsIgnoreCase(rawText.trim())) {
						cd = false;
					}

					if ((s2.trim()).equalsIgnoreCase(rawText.trim()) && row_height == 28.5 && colIndex == 1
							&& fontBold.equals("#NULL#")) {
						cd = false;
					}
					if ((s3.trim()).equalsIgnoreCase(rawText.trim()) && colIndex == 1) {
						cd = false;
					}
					
					if(cd==false){
					//if((s1.trim()).equalsIgnoreCase(rawText.trim())  || (s2.trim()).equalsIgnoreCase(rawText.trim()) && colIndex==1 || (s3.trim()).equalsIgnoreCase(rawText.trim())){
						ps1.setString(1, fileName);
						ps1.setString(2, "");

						if (colIndex == 1) {
							ps1.setString(3, rawText);
							// difcounter++;
							difcount.add(rawText);
						}
						if (colIndex == 2) {
							ps1.setString(4, rawText);
							// difcounter++;
							difcount.add(rawText);
						}

						if (colIndex == 4) {
							ps1.setString(6, rawText);
							// difcounter++;
							difcount.add(rawText);
						}

						ps1.setString(5, "--");

						if (difcount.size() == 3) {
							ps1.addBatch();
							ps1.executeBatch();
							ps1.clearBatch();
							// difcounter=0;
							difcount = new ArrayList<String>();
							cd = true;
						}
					//}
					}
			 }
			 
			 }
			 

		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		
	}*/
	
	
	public static void weightScreen(Connection conn3) throws SQLException{
		
		String sql2="INSERT INTO boa.WEIGHT_CONTROL_REQUIREMENTS_TEMP(file_name ,ITEM ,DECLARED_WT ,MAX_ALLOWABLE_VAR ,TARGET_WT ,DEFECTIVE_WT ,Weight_Notation ,Where_Sold ,Sales_Note) VALUES (?,?,?,?,?,?,?,?,?)";
		 PreparedStatement ps2 = null;

			ps2 = conn.prepareStatement(sql2);
			stmt3 = conn.createStatement();
			rs3=stmt3.executeQuery("select distinct(file_name) from boa.hershey_data_dump where file_name not in ('Natural Process Cocoa Powder.xlsx') order by file_name");

			while(rs3.next()){
				files2.add(rs3.getString("file_name"));
			}
			
			for(String file1:files2){
			//	System.out.println(file1);
				
				boolean goInside=true;
				boolean forSoldIn=true;
				boolean soldNotthere=true;
				int executionCount=0;
				int swap=0;

				stmt4=conn.createStatement();
				rs4=stmt4.executeQuery("select file_name,raw_text,row_index,col_index,row_height,col_span,font_bold,font_underline from  boa.hershey_data_dump where file_name='"+file1.replaceAll("'", "''")+"' order by row_index,col_index");
				StringBuilder sbr=new StringBuilder();

				List<Double>  colIndexCapture=new ArrayList<Double>();
				List<String> textCapture= new ArrayList<String>();
				
				while(rs4.next()){
					
					int	count=0;

				String fileName = (rs4.getString("file_name"));
				String rawText = (rs4.getString("raw_text"));
				int rowIndex = (rs4.getInt("row_index"));
				double colIndex = (rs4.getInt("col_index"));
				double rowHeight = (rs4.getDouble("row_height"));
				String dfWght = "Defective Weight";
				int colSpan = (rs4.getInt("col_span"));
				String fontBold = (rs4.getString("font_bold"));
				String fontUnderline = (rs4.getString("font_underline"));

					String pattern="Items Sold in ";
					String pattern2="Items sold in";

					
					if("WEIGHT CONTROL REQUIREMENTS".equals(rawText.trim().toUpperCase()) ){
						goInside=false;
					}
					
					if("NET WEIGHT".equalsIgnoreCase(rawText.trim().toUpperCase())){
						goInside=false;
					}
					
					
					if(goInside==false){
						
						if ((rawText.trim()).startsWith(pattern.trim())  || (rawText.trim()).startsWith(pattern2.trim())){
							
						}
						
					}
				}
			}
	}
	
}
