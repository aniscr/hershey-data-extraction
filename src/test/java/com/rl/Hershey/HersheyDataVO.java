package com.rl.Hershey;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import com.rl.Hershey.*;

@Data
public class HersheyDataVO {

	private String fileName;
	private String item;
	private String declaredWeight;
	private String maxAllowableVar;
	private String targetWeight;
	private String defectiveWeight;
	private String weightNotation;
	private String whereSold;
	private String salesNote;
	
	@Override
	public String toString() {
		return "HerseyPojo [file_name=" + fileName + ", ITEM=" + item + ", DECLARED_WT=" + declaredWeight + ",MAX_ALLOWABLE_VAR=" + maxAllowableVar + ",TARGET_WT=" + targetWeight + ",DEFECTIVE_WT=" + defectiveWeight + ", Weight_Notation=" + weightNotation + ", Where_Sold="+whereSold+", Sales_Note="+salesNote+"]";
	}
}
